#!/bin/bash
#########################################################
#                                                       #
#                       Dell PowerConnect               #
#                                                       #
# check_powerconnect                                    #
#       Version 1.5 (June 9th, 2016)                    #
#                                                       #
# Authored by Jason Leonard                             #
#       E-mail: jason_leonard@yahoo.com                 #
#                                                       #
# ----------------------------------------------------- #
#                                                       #
# Overview                                              #
#                                                       #
#       This plugin will check any Dell PowerConnect    #
# switch. As with most of my plugins, it came out of a  #
# specific need my company had when my boss decided to  #
# purchase PowerConnect switches for a remote site.     #
# (we are traditionally a Cisco shop). When searching   #
# for plugins, I found FMFreak's PowerConnect plugin,   #
# but it lacked ability to check environmentals. So,    #
# this script was designed to do precisely that. I      #
# also added the ability for it to report asset         #
# information.                                          #
#                                                       #
#       Most options for this program are fairly self-  #
# explanatory. The only thing worth nothing is that,    #
# in order to check a specific port, the -p parameter   #
# has to be used in addition to specifying -t port.     #
#                                                       #
#       The same applies for environmentals. I decided  #
# to allow them to be checked individually, but may     #
# add an option later to also check them all at once    #
# (like my HP server checks).                           #
#                                                       #
# This plugin is distributed under the GNU GPL license. #
# You may re-destribute only according to the terms of  #
# the GNU GPL v2.                                       #
#                                                       #
#########################################################

#########################################################
##                  GLOBAL VARIABLES                   ##
#########################################################
APPNAME=$(basename $0)
AUTHOR="Jason Leonard"
VERSION="1.5"

# Default settings for connection
COMMUNITY='public'
HOST_NAME='localhost'
SNMPVERSION='2c'

# State Variables
STATE_OK=0
STATE_WARN=1
STATE_CRIT=2
STATE_UNK=3

# Default Outputs
STATE=$STATE_OK
STATE_STRING=""
PERFDATA=""

# Default WARNING / CRITICAL for temperature checks
WARNING=65
CRITICAL=70

# Change this to modify the script's handling of how it
# separates each consumable/tray when multiple checks
# are output.
# SEPARATOR="\n"
SEPARATOR=','

# This is the character that tokenizes multiple arguments
# to the port check. I have this here so it's easy to
# change if I find the current character I have is buggy
# or a bad choice
ARG_TOKEN=','

#########################################################
#               Universal SNMP OIDS                     #
#########################################################
OID_productIdentificationPerUnitEntry="1.3.6.1.4.1.674.10895.3000.1.2.100.8.1"
OID_productIdentificationPerBoxIndex="1.3.6.1.4.1.674.10895.3000.1.2.100.8.1.1"
OID_productIdentificationDisplayName="1.3.6.1.4.1.674.10895.3000.1.2.100.1.0"
OID_productIdentificationVersion="1.3.6.1.4.1.674.10895.3000.1.2.100.4.0"
OID_ifOperStatus="1.3.6.1.2.1.2.2.1.8"
OID_envMonFanState="1.3.6.1.4.1.674.10895.3000.1.2.110.7.1.1.3"
OID_envMonFanStatusDescr="1.3.6.1.4.1.674.10895.3000.1.2.110.7.1.1.2"
OID_envMonSupplyStatusIndex="1.3.6.1.4.1.674.10895.3000.1.2.110.7.2.1.1"
OID_envMonSupplyState="1.3.6.1.4.1.674.10895.3000.1.2.110.7.2.1.3"
OID_envMonSupplyStatusDescr="1.3.6.1.4.1.674.10895.3000.1.2.110.7.2.1.2"
OID_envMonSupplySource="1.3.6.1.4.1.674.10895.3000.1.2.110.7.2.1.4"
OID_sysUpTime="1.3.6.1.2.1.1.3.0"
OID_productStatusGlobalStatus="1.3.6.1.4.1.674.10895.3000.1.2.110.1.0"

#########################################################
#                       print_version                   #
#########################################################
print_version() {

    echo "$APPNAME $VERSION"
        echo "$AUTHOR"
        echo ''

}

#########################################################
#                       print_usage                     #
#########################################################
print_usage(){

        echo ''
        echo 'Usage for SNMP v1/2c:'
        echo "  $APPNAME -H <host/IP> -t <type of check> [-C <SNMP community>] [-p <ports>] [-S <character>] [-w <warning value>] [-c <critical value>]"
        echo ''
        echo 'Usage for SNMP v3:'
        echo "  $APPNAME -H <host/IP> -t <check> -u <user> -x <protocol> -X <password> -a <protocol> -A <password> -l <security mode> [-p <ports>] [-S <character>] [-w <warning value>] [-c <critical value>]"
        echo ''

}

#########################################################
##                  print_help Function                ##
#########################################################
# Prints out user help and gives examples of proper     #
# plugin usage                                          #
#########################################################
function print_help () {

        print_version
        echo 'Description:'
        echo "$APPNAME is a Nagios plugin to check the status of various components of Dell PowerConnect switches."
        echo ''
        echo 'This plugin is not developped by the Nagios Plugin group.'
        echo 'Please do not e-mail them for support on this plugin.'
        echo ''
        echo 'For contact info, please read the plugin script file.'
        print_usage
        echo "---------------------------------------------------------------------"
        echo ''
        echo 'OPTIONS:'
        echo '  -H|--host'
        echo '          Host name or IP address to check. Default is: localhost. REQUIRED OPTION'
        echo '  -v|--snmpversion { 1 | 2c | 3 }'
        echo "          Specifies the SNMP version to use. Default is '2c'. REQUIRED OPTION"
        echo '  -C|--community'
        echo "          SNMP v2 community string with Read access. Default is 'public'. REQUIRED OPTION"
        echo '  -u|--user'
        echo '          SNMP v3 username'
        echo '  -l|--privlevel { noAuthNoPriv | authNoPriv | authPriv }'
        echo '          SNMP v3 privilege level'
        echo '  -x|--privprotocol { DES | AES }'
        echo '          SNMP v3 privacy protocol'
        echo '  -X|--privpassword'
        echo '          SNMP v3 privacy password'
        echo '  -a|--authprotocol { MD5 | SHA }'
        echo '          SNMP v3 authentication protocol'
        echo '  -A|--authpassword'
        echo '          SNMP v3 authentication password'
        echo '  -t|--type { assets | description | uptime | ports | port | health | temps | fans | psus }'
        echo '          The check you want to perform for the switch(es).'
        echo '          REQUIRED OPTION.'
        echo '  -p|--port <port(s)>'
        echo "          Specify port number, or list of ports (comma-separated) to be checked (only for the '-t port' option!)"
        echo '  -S|--separator <text string>'
        echo '          Assign a particular string as the separator for consumables.'
        echo '          Default is " " to conform to Nagios plugin development guidelines'
        echo '  -w|--warning'
        echo '          Assign WARNING value, in degrees C, for temperature checks'
        echo '  -c|--critical'
        echo '          Assign CRITICAL value, in degrees C, for temperature checks'
        echo '  -h|--help'
        echo '          Show this help screen'
        echo '  -V|--version'
        echo '          Show the current version of the plugin'
        echo ''
        echo 'CHECK TYPES:'
        echo '  assets:'
        echo '          Returns serial number and service tag of unit. '
        echo ''
        echo '  description:'
        echo '          Returns make and model of unit. Equivalent to checking sysDescr.'
        echo ''
        echo '  uptime:'
        echo '          How long the unit has been powered on. Equivalent to checking sysUptime.'
        echo ''
        echo '  ports:'
        echo '          Checks admin status of all ports. Reports number of ports UP and DOWN. Equivalent to checking ifOperStatus'
        echo ''
        echo '  port:'
        echo '          Admin status of specified ports. Reports number of ports UP and DOWN. '
        echo '          Ports to check can be specified as a range (x-y), as well as a comma-separated list (3,5,19).'
        echo ''
        echo '  health:'
        echo "          Global health status of the device(s). Will let you know something is wrong, but won't check what."
        echo ''
        echo '  temps:'
        echo '          Ambient temperature of the device(s).'
        echo '          WARNING and CRITICAL values are required! Default WARNING is 65 deg F and CRITICAL is 70 deg F.'
        echo ''
        echo '  fans:'
        echo '          Fan status of the device(s).'
        echo ''
        echo '  psus:'
        echo '          Power supply status of the device(s).'
        echo ''
        echo 'EXAMPLES:'
        echo "  $APPNAME -H hostname -C public -t port -p 42"
        echo '          to check status of port 42'
        echo "  $APPNAME -H hostname -C public -t psus"
        echo '          to check status of power supplies'
        echo "  $APPNAME -H hostname -C public -t temps -w 60 -c 70"
        echo '          to check thermal temperatures'
        echo ''

        exit $STATE_UNK

}

#########################################################
##                  CheckAssetInfo function            ##
#########################################################
# Returns serial # and service tag of device(s)         #
#########################################################
function CheckAssetInfo () {

        STACK_UNITS=`walk_snmp $OID_productIdentificationPerBoxIndex true`
        check_snmp_error "$?" "$STACK_UNITS"

        for CURRENT_UNIT in $STACK_UNITS; do
                SERIAL=`get_snmp $OID_productIdentificationPerUnitEntry.2.$CURRENT_UNIT`
                check_snmp_error "$?" "$SERIAL"

                # The service tag can also be located at the OID
                # SVC_TAG=`get_snmp $OID_productIdentificationPerUnitEntry.5.$CURRENT_UNIT`
                SVC_TAG=`get_snmp $OID_productIdentificationPerUnitEntry.4.$CURRENT_UNIT`
                check_snmp_error "$?" "$SVC_TAG"

                if [[ "$CURRENT_UNIT" -eq 1 ]]; then
                        STATE_STRING="Unit $CURRENT_UNIT - Serial # $SERIAL Service Tag $SVC_TAG"
                else
                        STATE_STRING="$STATE_STRING$SEPARATOR Unit $CURRENT_UNIT - Serial # $SERIAL Service Tag $SVC_TAG"
                fi
        done

}

#########################################################
##                CheckDescription function            ##
#########################################################
# Returns serial # and service tag of device(s)         #
#########################################################
function CheckDescription {

        MODEL=`get_snmp $OID_productIdentificationDisplayName`
        check_snmp_error "$?" "$MODEL"

        FIRMWARE=`get_snmp $OID_productIdentificationVersion`
        check_snmp_error "$?" "$FIRMWARE"

        STATE_STRING="$MODEL, Firmware $FIRMWARE"

}

#########################################################
##                  CheckPorts function                ##
#########################################################
# Checks general status of all ports in the stack       #
#########################################################
function CheckPorts () {

        PORT_STATUS=`walk_snmp $OID_ifOperStatus true`
        check_snmp_error "$?" "$PORT_STATUS"

        NUM_PORTS_DOWN=$(echo "$PORT_STATUS" | grep -c -e "down" -e "2")
        NUM_PORTS_UP=$(echo "$PORT_STATUS" | grep -c -e "up" -e "1")

        if [[ $NUM_PORTS_UP -ge 0 ]] && [[ $NUM_PORTS_DOWN -ge 0 ]]; then
                STATE_STRING="OK: Ports Up - $NUM_PORTS_UP, Ports Down - $NUM_PORTS_DOWN"
                PORTS_STATE_STATUS=$STATE_OK
        else
                STATE_STRING="UNKNOWN: received unexpected values for port status."
                PORTS_STATE_STATUS=$STATE_WARN
        fi

        return $PORTS_EXIT_STATUS

}

#########################################################
##                  CheckPort function                 ##
#########################################################
# Checks status of a single port in the stack           #
#########################################################
function CheckPort () {

        PORT_STATUS=`get_snmp $OID_ifOperStatus.$1`
        check_snmp_error "$?" "$PORT_STATUS"

        PORT_STATUS=$(echo "$PORT_STATUS" | tr -d " ")

        case "$PORT_STATUS" in
                "down(2)"|"down"|"2")
                        STATE_STRING="$STATE_STRING CRITICAL: Port $1 $PORT_STATUS.$SEPARATOR"
                        PORT_STATE_STATUS=$STATE_CRIT
                        ;;
                "up(1)"|"up"|"1")
                        STATE_STRING="$STATE_STRING OK: Port $1 $PORT_STATUS.$SEPARATOR"
                        PORT_STATE_STATUS=$STATE_OK
                        ;;
                "notPresent(6)"|"notPresent"|"6")
                        STATE_STRING="$STATE_STRING UNKNOWN: Port $1 $PORT_STATUS.$SEPARATOR"
                        PORT_STATE_STATUS=$STATE_WARN
                        ;;
                *)
                        STATE_STRING="$STATE_STRING UNKOWN: Port $1 not a valid port for this stack.$SEPARATOR"
                        PORT_STATE_STATUS=$STATE_WARN
                        ;;
        esac

        return $PORT_EXIT_STATUS

}

#########################################################
##                 CheckTemps function                 ##
#########################################################
# Checks status of temperatures for a stack             #
#########################################################
function CheckTemps () {
        TEMPS_STATE_STATUS=$STATE_OK
        DELL_OID="1.3.6.1.4.1.674.10895.5000.2.89.53.15.1.10"
        FASTPATH_OID="1.3.6.1.4.1.674.10895.5000.2.6132.1.1.43.1.8.1.3"
        RADLAN_OID="1.3.6.1.4.1.89.53.15.1.10"

        # Determine which OID tree is using the temperatures
        DELL_STATUS=`walk_snmp $DELL_OID false`
        check_snmp_error "$?" "$DELL_STATUS"

        USING_DELL=$(echo "$DELL_STATUS" | grep -v -c "OID")

        # For most switches
        FASTPATH_STATUS=`walk_snmp $FASTPATH_OID false`
        check_snmp_error "$?" "$FASTPATH_STATUS"

        USING_FASTPATH=$(echo "$FASTPATH_STATUS" | grep -v -c "OID")

        # For the DMZ switch
        RADLAN_STATUS=`walk_snmp $RADLAN_OID false`
        check_snmp_error "$?" "$RADLAN_STATUS"

        USING_RADLAN=$(echo "$RADLAN_STATUS" | grep -v -c "OID")

        if [[ $USING_DELL -gt 0 ]]; then
                STATUS_OID=$DELL_OID
                TEMPS_OID="1.3.6.1.4.1.674.10895.5000.2.89.53.15.1.9"
        fi

        if [[ $USING_FASTPATH -gt 0 ]]; then
                STATUS_OID=$FASTPATH_OID
                TEMPS_OID="1.3.6.1.4.1.674.10895.5000.2.6132.1.1.43.1.8.1.5"
        fi

        if [[ $USING_RADLAN -gt 0 ]]; then
                STATUS_OID=$RADLAN_OID
                TEMPS_OID="1.3.6.1.4.1.89.53.15.1.9"
        fi

        STACK_UNITS=`walk_snmp $OID_productIdentificationPerBoxIndex true`
        check_snmp_error "$?" "$STACK_UNITS"

        for CURRENT_UNIT in $STACK_UNITS; do

                UNIT_TEMPS=`walk_snmp $TEMPS_OID.$CURRENT_UNIT true`
                check_snmp_error "$?" "$UNIT_TEMPS"

                UNIT_STATUS=`walk_snmp $STATUS_OID.$CURRENT_UNIT true`
                check_snmp_error "$?" "$UNIT_STATUS"

                CURRENT_SENSOR=0
                NUM_SENSORS=$(echo "$UNIT_TEMPS" | wc -l)
                MAX_SENSOR=$[ $NUM_SENSORS-1 ];

                while [[ $CURRENT_SENSOR -le $MAX_SENSOR ]]; do

                        CURRENT_LINE=$[ $CURRENT_SENSOR+1 ];
                        SENSOR_STATUS=$(echo "$UNIT_STATUS" | awk NR=="$CURRENT_LINE")

                        if [[ $SENSOR_STATUS -gt 1 ]]; then
                                STATE_STRING="$STATE_STRING UNKNOWN: Unit $CURRENT_UNIT Sensor $CURRENT_SENSOR temperature cannot be read from the sensor!$SEPARATOR";
                                TEMPS_STATE_STATUS=$STATE_UNK
                        else
                                SENSOR_TEMP=$(echo "$UNIT_TEMPS" | awk NR=="$CURRENT_LINE")
                                if [ $SENSOR_TEMP -ge $CRITICAL ]; then
                                        STATE_STRING="$STATE_STRING CRITICAL: Unit $CURRENT_UNIT Sensor $CURRENT_SENSOR temperature of $SENSOR_TEMP degrees C exceeds threshold of $CRITICAL!$SEPARATOR";
                                        TEMPS_STATE_STATUS=$STATE_CRIT
                                else
                                        if [ $SENSOR_TEMP -lt $CRITICAL ] && [ $SENSOR_TEMP -ge $WARNING ]; then
                                                STATE_STRING="$STATE_STRING WARNING: Unit $CURRENT_UNIT Sensor $CURRENT_SENSOR temperature of $SENSOR_TEMP degrees C exceeds threshold of $WARNING!$SEPARATOR";
                                                TEMPS_STATE_STATUS=$STATE_WARN
                                        fi
                                fi
                        fi

                        PERFDATA="$PERFDATA Unit$CURRENT_UNIT-Sensor$CURRENT_SENSOR=$SENSOR_TEMP;$WARNING;$CRITICAL;"
                        CURRENT_SENSOR=$[ $CURRENT_SENSOR+1 ];

                done

                CURRENT_UNIT=$[ $CURRENT_UNIT+1 ];

        done

        if [[ $TEMPS_STATE_STATUS -eq $STATE_OK ]]; then
                STATE_STRING="OK: All temperature sensors reading below thresholds."
        fi

        return $TEMPS_STATE_STATUS

}

#########################################################
##                  CheckFans function                 ##
#########################################################
# Checks status of fans for a stack                     #
#########################################################
function CheckFans () {
        FAN_STATE_STATUS=$STATE_OK

        # This uses the Dell OID
        FAN_CONDITIONS=`walk_snmp $OID_envMonFanState true`
        check_snmp_error "$?" "$FAN_CONDITIONS"

        # This uses the Dell OID
        FAN_DESCRIPTIONS=`walk_snmp $OID_envMonFanStatusDescr true`
        check_snmp_error "$?" "$FAN_DESCRIPTIONS"

        STACK_UNITS=`walk_snmp $OID_productIdentificationPerBoxIndex true`
        check_snmp_error "$?" "$STACK_UNITS"

        for CURRENT_UNIT in $STACK_UNITS; do

                CURRENT_FAN=1
                NUM_FANS=$(echo "$FAN_CONDITIONS" | wc -l)

                while [[ $CURRENT_FAN -le $NUM_FANS ]]; do
                        FAN_DESCR=$(echo "$FAN_DESCRIPTIONS" | tr "_" " " | awk NR=="$CURRENT_FAN")
                        FAN_STATUS=$(echo "$FAN_CONDITIONS" | awk NR=="$CURRENT_FAN")

                        case "$FAN_STATUS" in
                                2)
                                        if [[ "$FAN_EXIT_STATUS" -lt 1 ]]; then
                                        # only set WARNING if nothing else has yet
                                                FAN_STATE_STATUS=$STATE_WARN
                                        fi
                                        STATE_STRING="$STATE_STRING WARNING: Unit $CURRENT_UNIT $FAN_DESCR compromised!$SEPARATOR"
                                        ;;
                                3)
                                        if [[ "$FAN_EXIT_STATUS" -lt 2 ]]; then
                                        # only set CRITICAL if nothing else has yet
                                                FAN_STATE_STATUS=$STATE_CRIT
                                        fi
                                        STATE_STRING="$STATE_STRING CRITICAL: Unit $CURRENT_UNIT $FAN_DESCR critical!$SEPARATOR"
                                        ;;
                                4)
                                        if [[ "$FAN_EXIT_STATUS" -lt 2 ]]; then
                                        # FAN is idle (has shutdown)
                                        # only set WARNING if nothing else has yet
                                                FAN_STATE_STATUS=$STATE_WARN
                                        fi
                                        STATE_STRING="$STATE_STRING WARNING: Unit $CURRENT_UNIT $FAN_DESCR idle!$SEPARATOR"
                                        ;;
                                5)
                                        if [[ "$FAN_EXIT_STATUS" -eq 0 ]]; then
                                                FAN_STATE_STATUS=$STATE_UNK
                                        fi
                                        STATE_STRING="$STATE_STRING UNKNOWN: Unit $CURRENT_UNIT $FAN_DESCR not present.$SEPARATOR"
                                        ;;
                                6)
                                        if [[ "$FAN_EXIT_STATUS" -lt 2 ]]; then
                                        # only set CRITICAL if nothing else has yet
                                                FAN_STATE_STATUS=$STATE_CRIT
                                        fi
                                        STATE_STRING="$STATE_STRING CRITICAL: Unit $CURRENT_UNIT $FAN_DESCR not functioning.$SEPARATOR"
                                        ;;
                        esac

                        CURRENT_FAN=$[ $CURRENT_FAN+1 ];
                done

        done

        if [[ $FAN_EXIT_STATUS -eq $STATE_OK ]]; then
                STATE_STRING="OK: All fans operating normally."
        fi

        return $FAN_EXIT_STATUS

}

#########################################################
##                  CheckPSUs function                 ##
#########################################################
# Checks status of power supplies for a stack           #
#########################################################
function CheckPSUs () {

        UNIT_NUMBERS=`walk_snmp $OID_envMonSupplyStatusIndex true`
        check_snmp_error "$?" "$UNIT_NUMBERS"

        PSU_STATE_STATUS=$STATE_OK

        PSU_CONDITIONS=`walk_snmp $OID_envMonSupplyState true`
        check_snmp_error "$?" "$PSU_CONDITIONS"

        PSU_DESCRIPTIONS=`walk_snmp $OID_envMonSupplyStatusDescr true`
        check_snmp_error "$?" "$PSU_DESCRIPTIONS"

        PSU_TYPES=`walk_snmp $OID_envMonSupplySource true`
        check_snmp_error "$?" "$PSU_TYPES"

        STACK_UNITS=`walk_snmp $OID_productIdentificationPerBoxIndex true`
        check_snmp_error "$?" "$STACK_UNITS"

        for CURRENT_UNIT in $STACK_UNITS; do

                CURRENT_PSU=1
                NUM_PSUS=$(echo "$PSU_CONDITIONS" | wc -l)

                while [[ $CURRENT_PSU -le $NUM_PSUS ]]; do

                        PSU_DESCR=$(echo "$PSU_DESCRIPTIONS" | tr "_" " " | awk NR=="$CURRENT_PSU")
                        PSU_TYPE=$(echo "$PSU_TYPES" | awk NR=="$CURRENT_PSU" )

                        case "$PSU_TYPE" in
                                1) PSU_DESCR="$PSU_DESCR (unknown source)"
                                ;;
                                2) PSU_DESCR="$PSU_DESCR (ac power)"
                                ;;
                                3) PSU_DESCR="$PSU_DESCR (dc power)"
                                ;;
                                4) PSU_DESCR="$PSU_DESCR (external power)"
                                ;;
                                5) PSU_DESCR="$PSU_DESCR (internal power)"
                                ;;
                        esac

                        PSU_STATUS=$(echo "$PSU_CONDITIONS" | awk NR=="$CURRENT_PSU")

                        case "$PSU_STATUS" in
                                2)
                                if [[ "$PSU_EXIT_STATUS" -lt 1 ]]; then
                                        # only set WARNING if nothing else has yet
                                                PSU_STATE_STATUS=$STATE_WARN
                                        fi
                                        STATE_STRING="$STATE_STRING WARNING: Unit $CURRENT_UNIT $PSU_DESCR compromised!$SEPARATOR"
                                        ;;
                                3)
                                        if [[ "$PSU_EXIT_STATUS" -lt 2 ]]; then
                                        # only set CRITICAL if nothing else has yet
                                                PSU_STATE_STATUS=$STATE_CRIT
                                        fi
                                        STATE_STRING="$STATE_STRING CRITICAL: Unit $CURRENT_UNIT $PSU_DESCR critical!$SEPARATOR"
                                        ;;
                                4)
                                        if [[ "$PSU_EXIT_STATUS" -lt 2 ]]; then
                                        # only set CRITICAL if nothing else has yet
                                                PSU_STATE_STATUS=$STATE_CRIT
                                        fi
                                        STATE_STRING="$STATE_STRING CRITICAL: Unit $CURRENT_UNIT $PSU_DESCR critical! Please shutdown the unit!$SEPARATOR"
                                        ;;
                                #5)
                                #       if [[ "$PSU_EXIT_STATUS" -eq 0 ]]; then
                                #               PSU_STATE_STATUS=$STATE_UNK
                                #       fi
                                #       STATE_STRING="$STATE_STRING UNKNOWN: Unit $CURRENT_UNIT $PSU_DESCR not present.$SEPARATOR"
                                #       ;;
                                6)
                                        if [[ "$PSU_EXIT_STATUS" -lt 2 ]]; then
                                        # only set CRITICAL if nothing else has yet
                                                PSU_STATE_STATUS=$STATE_CRIT
                                        fi
                                        STATE_STRING="$STATE_STRING CRITICAL: Unit $CURRENT_UNIT $PSU_DESCR not functioning.$SEPARATOR"
                                        ;;
                        esac

                        CURRENT_PSU=$[ $CURRENT_PSU+1 ]

                done

        done

        if [[ $PSU_EXIT_STATUS -eq $STATE_OK ]]; then
                STATE_STRING="OK: All power supplies operating normally."
        fi

        return $PSU_EXIT_STATUS

}

#########################################################
#                       Function: CheckUptime           #
#########################################################
function CheckUptime() {

        UPTIME_STATE=$STATE_OK

        UPTIME=`get_snmp2 $OID_sysUpTime`
        TICKS=$(echo "$UPTIME" | cut -d " " -f 2 | sed 's/(//g'| sed 's/)//g')
        DAYS=$(echo "$UPTIME" | cut -d " " -f 3)
        # 259200 seconds is 72 hours???
        if [ $TICKS -lt 259200 ] && [ "$UPTIME" != '' ]; then
                STATE_STRING="WARNING: Uptime = $DAYS days. Unit restarted once.";
                UPTIME_STATE=$STATE_WARN
        else
                if [ $TICKS -gt "259200" ] && [ "$UPTIME" != '' ]; then
                        STATE_STRING="OK: Uptime = $DAYS days."
                else
                        STATE_STRING="CRITICAL: Unit may be down!";
                        UPTIME_STATE=$STATE_CRIT
                fi
        fi

        return $UPTIME_STATE

}

#########################################################
#               Subroutine: walk_snmp                   #
#########################################################
walk_snmp(){

        if [ $2 = true ]; then
                OUTPUT_OPTIONS="-Oavq"
        else
                OUTPUT_OPTIONS="-Oa"
        fi

        if [[ $SNMPVERSION = 3 ]]; then
                RESULT_TEXT=`snmpwalk -v $SNMPVERSION $OUTPUT_OPTIONS -u $SNMPUSER -l $PRIVILEGELEVEL -x $PRIVACYPROTOCOL -X $PRIVACYPASSWORD -a $AUTHPROTOCOL -A $AUTHPASSWORD $HOST_NAME $1`
                RESULT_CODE=$?
        else
                # Check if community was also set
                RESULT_TEXT=`snmpwalk -v $SNMPVERSION $OUTPUT_OPTIONS -c $COMMUNITY $HOST_NAME $1`
                RESULT_CODE=$?
        fi

        if [[ $RESULT_CODE -ne 0 ]]; then
                echo "Plugin $APPNAME failure - snmpwalk command error."
                echo "$RESULT_TEXT"
                exit $STATE_UNK
        fi

        if [ $2 = true ]; then
                echo "$RESULT_TEXT" | sed -e 's/^[[:space:]]*//' | tr -d "\""
        else
                echo "$RESULT_TEXT"
        fi

}

#########################################################
#               Subroutine: get_snmp                    #
#########################################################
get_snmp(){

        if [[ $SNMPVERSION = 3 ]]; then
                RESULT_TEXT=`snmpget -v $SNMPVERSION -Oavq -u $SNMPUSER -l $PRIVILEGELEVEL -x $PRIVACYPROTOCOL -X $PRIVACYPASSWORD -a $AUTHPROTOCOL -A $AUTHPASSWORD $HOST_NAME $1`
                RESULT_CODE=$?
        else
                # Check if community was also set
                RESULT_TEXT=`snmpget -v $SNMPVERSION -Oavq -c $COMMUNITY $HOST_NAME $1`
                RESULT_CODE=$?
        fi

        if [[ $RESULT_CODE -ne 0 ]]; then
                echo "Plugin $APPNAME failure - snmpget command error."
                echo $RESULT_TEXT | tr -d "\""
                exit $STATE_UNK
        fi

        echo "$RESULT_TEXT" | tr -d "\""

}

#########################################################
#               Subroutine: get_snmp                    #
#########################################################
get_snmp2(){

        if [[ $SNMPVERSION = 3 ]]; then
                RESULT_TEXT=`snmpget -v $SNMPVERSION -Oav -u $SNMPUSER -l $PRIVILEGELEVEL -x $PRIVACYPROTOCOL -X $PRIVACYPASSWORD -a $AUTHPROTOCOL -A $AUTHPASSWORD $HOST_NAME $1`
                RESULT_CODE=$?
        fi

        if [[ $RESULT_CODE -ne 0 ]]; then
                echo "Plugin $APPNAME failure - snmpget command error."
                echo $RESULT_TEXT | tr -d "\""
                exit $STATE_UNK
        fi

        echo "$RESULT_TEXT" | tr -d "\""

}

#########################################################
#               Subroutine: check_snmp_error                    #
#########################################################
# Tests errors returned by function operations          #
#########################################################
check_snmp_error(){

         if [[ $1 -ne 0 ]]; then
                echo $2
                exit $STATE_UNK
        fi

}

#########################################################
##                      MAIN CODE                      ##
#########################################################

# Check that all required binaries for the script are available
#       EXIT with an UNKNOWN status if not
binaries="snmpwalk snmpget cut tr sed grep awk wc"

for required_binary in $binaries
do
        which $required_binary > /dev/null
        if [ "$?" != '0' ];then
                echo "UNKNOWN: $APPNAME: No usable '$required_binary' binary in '$PATH'"
                exit $STATE_UNK
        fi
done

# Check to see if any parameters were passed
if [[ $# -eq 0 ]]; then
        print_usage
        exit $STATE_UNK
fi

# Parse our options as passed, and make sure things are peachy
while test -n "$1"; do

        case "$1" in
                --host|-H)
                        HOST_NAME=$2
                        shift
                        ;;
                --comunity|-C)
                        COMMUNITY=$2
                        shift
                        ;;
                --snmpversion|-v)
                        SNMPVERSION=$2
                        shift
                        ;;
                --user|-u)
                        SNMPUSER=$2
                        shift
                        ;;
                --privelegelevel|-l)
                        PRIVILEGELEVEL=$2
                        shift
                        ;;
                --authprotocol|-a)
                        AUTHPROTOCOL=$2
                        shift
                        ;;
                --authpassword|-A)
                        AUTHPASSWORD=$2
                        shift
                        ;;
                --privacyprotocol|-x)
                        PRIVACYPROTOCOL=$2
                        shift
                        ;;
                --privacypassword|-X)
                        PRIVACYPASSWORD=$2
                        shift
                        ;;
                --type|-t)
                        CHECK_TYPE=$2
                        shift
                        ;;
                --port|-p)
                        PORT_LIST=$2
                        shift
                        ;;
                --separator|-S) # Assign separator
                        SEPARATOR="$2"
                        shift
                        ;;
                --warning|-w) # Assign WARNING threshold
                        WARNING=$2
                        shift
                        ;;
                --critical|-c) # Assign CRITICAL threshold
                        CRITICAL=$2
                        shift
                        ;;
                --help|-h)
                        print_help
                        ;;
                --version|-V)
                        print_version
                        exit $STATE
                        ;;
                  *)
                        echo "Unknown argument: $1"
                        print_usage
                        exit $STATE_UNK
                        ;;

        esac

        shift

done

# Make sure all necessary arguments were given; EXIT with an UNKNOWN status if not
if [ ! -z $CHECK_TYPE ]; then
        # Determine the check to perform
        case "$CHECK_TYPE" in
                "assets")
                        CheckAssetInfo
                        ;;
                "description")
                        CheckDescription
                        ;;
                "uptime")
                        CheckUptime
                        STATE=$?
                        ;;
                "ports")
                        CheckPorts
                        STATE=$?
                        ;;
                "port")
                        # be sure CHECK_TYPE is not null first!!!
                        for CURRENT_PORT in $(echo "$PORT_LIST" | tr "$ARG_TOKEN" "\n")
                        do
                                CheckPort "$CURRENT_PORT"
                                CURRENT_PORT_STATUS=$?

                                if [ "$CURRENT_PORT_STATUS" -gt "$STATE" ]; then
                                        STATE="$CURRENT_PORT_STATUS"
                                fi
                        done
                        ;;
                "health")
                        GLOBALHEALTH=`get_snmp $OID_productStatusGlobalStatus`
                        check_snmp_error "$?"

                        case "$GLOBALHEALTH" in
                                3)
                                STATE_STRING="OK: System hardware is okay."
                                STATE=$STATE_OK
                                ;;
                                4)
                                STATE_STRING="WARNING: At least one power supply is not functional, or hardware reported error."
                                STATE=$STATE_WARN
                                ;;
                                5)
                                STATE_STRING="CRITICAL: At least one fan is not functional, or hardware has fatal errors."
                                STATE=$STATE_CRIT
                                ;;
                        esac
                        ;;
                "temps")
                        CheckTemps
                        STATE=$?
                        ;;
                "fans")
                        CheckFans
                        STATE=$?
                        ;;
                "psus")
                        CheckPSUs
                        STATE=$?
                        ;;
                *)
                        STATE_STRING="UNKNOWN: No valid check type specified to -t parameter"
                        STATE=$STATE_UNK
                        ;;
        esac

else
        echo "Command incomplete!"
        STATE=$STATE_UNK
fi

# If the program hasn't exited already, then a check was run okay and we can quit.
if [ "$PERFDATA" == "" ]; then
        echo "$STATE_STRING"
else
        echo "$STATE_STRING|$PERFDATA"
fi

exit $STATE
